package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by Acer on 11.11.2016.
 */
public class Task3TripSummaryForm {

    private WebDriver driver;
    private WebElement selectBtnElement;
    private By selectBtnLocator = By.xpath("//button[@id='tripSummarySubmitBtn']");


    public Task3TripSummaryForm(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(100000, TimeUnit.MILLISECONDS);
        System.out.format("Get access to Trip Summary page %n");
    }

    /**
     * Click on button to continue next page
     */
    public void clickOnBtnContinue() {
        (new WebDriverWait(driver, 60)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(selectBtnLocator).isDisplayed();
            }
        });

        selectBtnElement= driver.findElement(selectBtnLocator);
        selectBtnElement.click();
        System.out.format("Click on 'Continue' button %n");

    }
}

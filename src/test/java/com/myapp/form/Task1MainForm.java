package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Acer on 10.11.2016.
 */
public class Task1MainForm {

    private WebDriver driver;
    private By searchTextBoxLocator = By.id("search_from_str");
    private By searchBtnLocator = By.name("search");
    private WebElement searchInputElement;
    private WebElement searchBtnElement;

    public Task1MainForm(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * В методе осуществляется ввод заданного текста запроса text в поле ввода и
     * нажатие на кнопку "Найти". По нажатию на кнопку "Найти" осуществляется
     * переход к странице с результатами поиска.
     *
     * @param text
     *            текст запроса
     */

    public void search(String text) {
        searchInputElement = driver.findElement(searchTextBoxLocator);
        searchInputElement.clear();
        searchInputElement.sendKeys(text);
        System.out.format("Ввели запрос \"%s\" в строку поиска%n", text);

        searchBtnElement = driver.findElement(searchBtnLocator);
        searchBtnElement.click();
        System.out.format("Нажали кнопку \"Найти\"%n");

    }
}

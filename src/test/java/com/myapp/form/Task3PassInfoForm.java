package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Acer on 11.11.2016.
 */
public class Task3PassInfoForm {

    private WebDriver driver;
    private WebElement firstNameElement;
    private WebElement lastNameElement;
    private WebElement prefixBtnElement;
    private WebElement genderBtnElement;
    private WebElement monthBtnElement;
    private WebElement dayBtnElement;
    private WebElement yearBtnElement;
    private By firstNameLocator = By.name("firstName0");
    private By lastNameLocator = By.name("lastName0");
    private By prefixBtnLocator = By.xpath("//span[@id='prefix0-button']/span");
    private By prefixLocator = By.xpath("//ul[@id='prefix0-menu']/descendant::li");
    private By genderBtnLocator = By.xpath("//span[@id='gender0-button']/span");
    private By genderLocator = By.xpath("//ul[@id='gender0-menu']/descendant::li");
    private By monthBtnLocator = By.xpath("//span[@id='month0-button']/span");
    private By monthLocator = By.xpath("//ul[@id='month0-menu']/descendant::li");
    private By dayBtnLocator = By.xpath("//span[@id='day0-button']/span");
    private By dayLocator = By.xpath("//ul[@id='day0-menu']/descendant::li");
    private By yearBtnLocator = By.xpath("//span[@id='year0-button']/span");
    private By yearLocator = By.xpath("//ul[@id='year0-menu']/descendant::li");


    //=======DEFAUL PARAMETERS======//
    private String prefix = "Miss";
    private String firstname = "Vasil";
    private String lastname = "Pupkin";
    private String gender = "Male";
    private String month = "December";
    private String phonenumber = "123456789";
    private String email = "12345@mail.by";
    private String day = "25";
    private String year = "1980";


    public Task3PassInfoForm(WebDriver driver) {
        this.driver = driver;
        System.out.format("Get access to Passenger Info page %n");

    }

    /**
     * In this method fill in field prefix.
     * Prefix can be change on any from li list
     * @param prefix
     */
    public void fillPrefixBox(String prefix) {
        prefixBtnElement = driver.findElement(prefixBtnLocator);
        prefixBtnElement.click();
        String flag = "false";
        List<WebElement> searchResults = driver.findElements(prefixLocator);

        for (int i = 0; i < searchResults.size(); i++) {
            if (searchResults.get(i).getText().equals(prefix)) {
                driver.findElement(By.xpath("//ul[@id='prefix0-menu']/li[" + (i + 1) + "]")).click();
                flag = "true";
                break;
            }
        }

        if (flag.equals("false")) {
            System.out.format("Please check if the %s prefix exist or correct %n", prefix);
        } else {
            System.out.format("SET PREFIX: %s%n", prefix);
        }
    }

    /**
     * In this method fill in fields FIRST NAME/LAST NAME.
     * Parameters can be change on any from list
     * @param firstname
     * @param lastname
     */
    public void fillFirstAndLastNameBox(String firstname, String lastname) {
        //============Field for FIRSTNAME======================//
        firstNameElement = driver.findElement(firstNameLocator);
        firstNameElement.click();
        firstNameElement.clear();
        firstNameElement.sendKeys(firstname);
        System.out.format("SET FIRSTNAME: %s%n", firstname);
        //============END Field for FIRSTNAME=================//


        //============Field for LASTNAME=======================//
        lastNameElement = driver.findElement(lastNameLocator);
        lastNameElement.click();
        lastNameElement.clear();
        lastNameElement.sendKeys(lastname);
        System.out.format("SET LASTNAME: %s%n", lastname);
        //============END Field for LASTNAME=================//

    }

    /**
     * In this method fill in Gender
     * Parameter can be change on any from list
     * @param gender
     */
    public void fillGenderBox(String gender) {

        genderBtnElement = driver.findElement(genderBtnLocator);
        genderBtnElement.click();
        String flag = "false";
        List<WebElement> searchResults = driver.findElements(genderLocator);

        for (int i = 0; i < searchResults.size(); i++) {
            if (searchResults.get(i).getText().equals(gender)) {
                driver.findElement(By.xpath("//ul[@id='gender0-menu']/li[" + (i + 1) + "]")).click();
                flag = "true";
                break;
            }
        }

        if (flag.equals("false")) {
            System.out.format("Please check if the %s Gender exist or correct %n", gender);
        } else {
            System.out.format("SET Gender: %s%n", gender);
        }

    }

    /**
     * In this method fill in Month
     * Parameter can be change on any from list
     * @param month
     */
    public void fillMonthBox(String month) {

        monthBtnElement = driver.findElement(monthBtnLocator);
        monthBtnElement.click();
        String flag = "false";
        List<WebElement> searchResults = driver.findElements(monthLocator);

        for (int i = 0; i < searchResults.size(); i++) {
            if (searchResults.get(i).getText().equals(month)) {
                driver.findElement(By.xpath("//ul[@id='month0-menu']/li[" + (i + 1) + "]")).click();
                flag = "true";
                break;
            }
        }

        if (flag.equals("false")) {
            System.out.format("Please check if the %s month exist or correct %n", month);
        } else {
            System.out.format("SET Month: %s%n", month);
        }

    }

    /**
     * In this method fill in Day
     * Parameter can be change on any from list
     * @param day
     */
    public void fillDayBox(String day) {

        dayBtnElement = driver.findElement(dayBtnLocator);
        dayBtnElement.click();
        String flag = "false";
        List<WebElement> searchResults = driver.findElements(dayLocator);

        for (int i = 0; i < searchResults.size(); i++) {
            if (searchResults.get(i).getText().equals(day)) {
                driver.findElement(By.xpath("//ul[@id='day0-menu']/li[" + (i + 1) + "]")).click();
                flag = "true";
                break;
            }
        }

        if (flag.equals("false")) {
            System.out.format("Please check if the %s Day exist or correct %n", day);
        } else {
            System.out.format("SET Day: %s%n", day);
        }

    }

    /**
     * In this method fill in YEAR
     * Parameter can be change on any from list
     * @param year
     */

    public void fillYearBox(String year) {

        yearBtnElement = driver.findElement(yearBtnLocator);
        yearBtnElement.click();
        String flag = "false";
        List<WebElement> searchResults = driver.findElements(yearLocator);

        for (int i = 0; i < searchResults.size(); i++) {
            if (searchResults.get(i).getText().equals(year)) {
                driver.findElement(By.xpath("//ul[@id='year0-menu']/li[" + (i + 1) + "]")).click();
                flag = "true";
                break;
            }
        }

        if (flag.equals("false")) {
            System.out.format("Please check if the %s year exist or correct %n", year);
        } else {
            System.out.format("SET Year: %s%n", year);
        }

    }

    /**
     * Fill in Telephone Number
     * @param phonenumber
     */
    public void fillPhNumberBox(String phonenumber) {
        driver.findElement(By.id("telephoneNumber0")).sendKeys(phonenumber);
        System.out.format("SET PHONE: %s %n", phonenumber);

    }

    /**
     * Fill in email
     * @param email
     */
    public void setAndConfirmEmailBox(String email) {
        driver.findElement(By.id("email")).sendKeys(email);
        System.out.format("SET EMAIL: %s%n", email);

        driver.findElement(By.id("reEmail")).sendKeys(email);
        System.out.format("CONFIRM EMAIL: %s%n", email);

    }

    /**
     * Turn off emergancy fields
     */
    public void turnOFFEmergencyContact() {
        WebElement element = driver.findElement(By.id("declineContactN_0"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
        System.out.format("Select NO in emergency contact info %n");
    }


    public void clickONContinueBtn() {
        driver.findElement(By.id("paxReviewPurchaseBtn")).click();
        System.out.format("click on continue button %n");
    }


    /**
     * At this method Fill in all required
     * Passenger Information fields
     */
    public void fillDefaultFieldsInPassInfoPage() {
        fillPrefixBox(prefix);
        fillFirstAndLastNameBox(firstname, lastname);
        fillGenderBox(gender);
        fillMonthBox(month);
        fillDayBox(day);
        fillYearBox(year);
        fillPhNumberBox(phonenumber);
        setAndConfirmEmailBox(email);
        turnOFFEmergencyContact();
        clickONContinueBtn();

    }

}


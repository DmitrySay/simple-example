package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Acer on 11.11.2016.
 */
public class Task3TicketsForm {

    private WebDriver driver;

    public Task3TicketsForm(WebDriver driver) {
        this.driver = driver;
        System.out.format("Get access to Tickets selection page %n");
    }

    /**
     * Select the 1st ticket
     */
    public void selectTicket() {

        (new WebDriverWait(driver, 120)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(By.xpath("//button[@id='0_0_0']")).isDisplayed();
            }
        });
        driver.findElement(By.xpath("//button[@id='0_0_0']")).click();
        System.out.format("Click on select button with 1st ticket %n");
    }
}



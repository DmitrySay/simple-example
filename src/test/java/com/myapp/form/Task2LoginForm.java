package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

/**
 * Created by Acer on 11.11.2016.
 */
public class Task2LoginForm {


    private WebDriver driver;
    private WebElement loginElement;
    private WebElement buttonNextElement;
    private By loginLocator = By.id("Email");
    private By buttonNextLocator = By.id("next");


    public Task2LoginForm(WebDriver driver) {
        this.driver = driver;
        System.out.format("Получили доступ к странице для ввода логина%n");
    }


    /**
     * В методе осуществляется ввод заданного логина
     *
     * @param login
     */
    public void setLogin(String login) {
        loginElement = driver.findElement(loginLocator);
        loginElement.clear();
        loginElement.sendKeys(login);
        System.out.format("Ввели логин \"%s\" в строку %n", login);
        buttonNextElement = driver.findElement(buttonNextLocator);
        buttonNextElement.click();
        System.out.format("Нажали Далее%n");

    }

}

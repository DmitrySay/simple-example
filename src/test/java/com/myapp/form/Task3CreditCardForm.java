package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Acer on 12.11.2016.
 */
public class Task3CreditCardForm {

    private WebDriver driver;
    private WebElement btnElement;
    private By btnLocator = By.id("continue_button");

    public Task3CreditCardForm(WebDriver driver) {
        this.driver = driver;
        System.out.format("Get access to Credit Card Info page %n");
    }

    /**
     * Method check if button
     * Complete Purchase is active
     */
    public void checkIfBtnActive() {

        btnElement=driver.findElement(btnLocator);
        btnElement.isEnabled();
        btnElement.isDisplayed();
        System.out.println("Button is active 'Complete Purchase'");

    }

}

package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

/**
 * Created by Acer on 11.11.2016.
 */
public class Task3MainForm {

    private WebDriver driver;
    private WebElement fromElement;
    private WebElement toElement;
    private WebElement departDateElement;
    private WebElement returnDateElement;
    private WebElement exactDatesElement;
    private WebElement cashBtnElement;
    private WebElement findFlightsSubmitBtnElement;
    private By fromLocator = By.xpath("//input[@id='originCity']");
    private By toLocator = By.xpath("//input[@id='destinationCity']");
    private By departDateLocator = By.xpath("//input[@id='departureDate']");
    private By returnDateLocator = By.xpath("//input[@id='returnDate']");
    private By exactDatesLocator = By.xpath("//label[@id='exactDaysBtn']/span");
    private By cashBtnLocator = By.xpath("//label[@id='cashBtn']/span");
    private By findFlightsSubmitBtnLocator = By.name("findFlightsSubmit");


    public Task3MainForm(WebDriver driver) {
        this.driver = driver;
        System.out.format("Get access to MainPage %n");
    }

    /**
     * Select Flight tab
     */
    public void clickOnFlightTab() {
        driver.findElement(By.xpath("//a[@id='book-air-content-trigger']")).click();
        System.out.format("Click on 'FLIGHT' tab under 'Book a trip' booking widget %n");
    }

    /**
     * Select Flight type =RoundTrip
     */
    public void clickOnRoundTrip() {
        driver.findElement(By.xpath("//label[@id='roundTripBtn']/span")).click();
        System.out.format("Click on 'ROUND TRIP' tab under 'Book a trip' booking widget %n");
    }

    /**
     * Select Date type =ExactDates
     */
    public void clickOnExactDates() {
        exactDatesElement = driver.findElement(exactDatesLocator);
        exactDatesElement.click();
        System.out.format("Click on 'ROUND TRIP' tab under 'Book a trip' booking widget %n");
    }

    /**
     *Select Price type =SHOW PRICE IN:Money
     */
    public void clickOnShowPriceInMoney() {
        cashBtnElement = driver.findElement(cashBtnLocator);
        cashBtnElement.click();
        System.out.format("Click on 'SHOW PRICE IN Money' tab under 'Book a trip' booking widget %n");
    }

    /**
     * Click on submit button
     */
    public void clickOnFindFlightsSubmit() {
        findFlightsSubmitBtnElement = driver.findElement(findFlightsSubmitBtnLocator);
        findFlightsSubmitBtnElement.click();
        System.out.format("Click on 'FindFlightsSubmit' in booking widget %n");

    }

    /**
     * Fill in FROM/TO fly destination
     * @param from
     * @param to
     */
    public void setFromAndToFields(String from, String to) {

        fromElement = driver.findElement(fromLocator);
        fromElement.click();
        fromElement.clear();
        fromElement.sendKeys(from);
        System.out.format("SET FROM: %s%n", from);

        toElement = driver.findElement(toLocator);
        toElement.click();
        toElement.clear();
        toElement.sendKeys(to);
        System.out.format("SET TO: %s%n", to);
    }

    /**
     * Fill in DepartDate/ReturnDate
     * @param departDate
     * @param returnDate
     */
    public void setDepartDateAndReturnDate(String departDate, String returnDate) {

        departDate.replaceAll("[\\D]", "");
        returnDate.replaceAll("[\\D]", "");

        departDateElement = driver.findElement(departDateLocator);
        departDateElement.click();
        departDateElement.clear();
        departDateElement.sendKeys(departDate);
        System.out.format("SET DEPART DATE: %s%n", departDate);

        returnDateElement = driver.findElement(returnDateLocator);
        returnDateElement.click();
        returnDateElement.clear();
        returnDateElement.sendKeys(returnDate);
        System.out.format("SET RETURN DATE: %s%n", returnDate);
    }
}


package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by Acer on 11.11.2016.
 */
public class Task2PasswordForm {


    private WebDriver driver;
    private WebElement passElement;
    private WebElement buttonNextElement;
    private By passLocator = By.id("Passwd");
    private By buttonNextLocator = By.id("signIn");


    public Task2PasswordForm(WebDriver driver) {
        this.driver = driver;
        System.out.format("Получили доступ к странице для ввода пароля%n");
    }

    /**
     * В методе осуществляется ввод заданного пароля
     * @param password
     */
    public void setPassword(String password) {

        (new WebDriverWait(driver, 60)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(passLocator).isDisplayed();
            }
        });

        passElement = driver.findElement(passLocator);
        passElement.clear();
        passElement.sendKeys(password);
        System.out.format("Ввели пароль \"%s\" в строку %n", password);
        buttonNextElement = driver.findElement(buttonNextLocator);
        buttonNextElement.click();
        System.out.format("Нажали Войти%n");
        driver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);

    }
}

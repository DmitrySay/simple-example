package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Acer on 10.11.2016.
 */
public class Task2MainForm {

    private WebDriver driver;

    public Task2MainForm(WebDriver driver) {
        this.driver = driver;
        System.out.format("Получили доступ к главной странице%n");
    }

    /**
     * В методе осуществляется поиск и переход по линку "Почта"
     */
    public void navigateToMail() {

        WebElement element = driver.findElement(By.linkText("Почта"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
        System.out.format("Нажали кнопку \"Почта\"%n");
    }
}

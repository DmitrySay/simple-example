package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by Acer on 11.11.2016.
 */
public class Task2MailForm {

    private WebDriver driver;
    private String title = "";


    public Task2MailForm(WebDriver driver) {
        this.driver = driver;
        System.out.format("Получили доступ к почте%n");
        driver.manage().timeouts().implicitlyWait(150000, TimeUnit.MILLISECONDS);
    }

    /**
     * В методе осуществляется создание и отправка email
     *
     * @param email   -кому отправляем
     * @param topic   -тема письма
     * @param message -текст письма
     */
    public void setEmailTopicMessageAndSend(String email, String topic, String message) {

        driver.findElement(By.xpath("//div[text()= 'НАПИСАТЬ']")).click();
        System.out.format("Кликнули по Написать %n");
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);

        driver.findElement(By.xpath("//textarea[contains(@aria-label, 'Кому')]")).click();
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
        driver.findElement(By.xpath("//textarea[contains(@aria-label, 'Кому')]")).sendKeys(email);
        System.out.format("Ввели email%n");

        driver.findElement(By.name("subjectbox")).click();
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
        driver.findElement(By.name("subjectbox")).sendKeys(topic);
        System.out.format("Ввели тему%n");

        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
        driver.findElement(By.xpath("(//*[@aria-label='Тело письма'])[2]")).click();
        driver.findElement(By.xpath("(//*[@aria-label='Тело письма'])[2]")).sendKeys(message);
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
        System.out.format("Ввели отправляемое сообщение %n");

        driver.findElement(By.xpath("//div[text()='Отправить']")).click();
        driver.manage().timeouts().implicitlyWait(100000, TimeUnit.MILLISECONDS);
        System.out.format("Нажали отправить сообщение %n");
    }

    /**
     * В методе осуществляется выход из Почты
     */
    public void logoutFromMail() {
        WebElement element = driver.findElement(By.id("gb_71"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
        System.out.format("Вышли из Почты %n");
    }

    /**
     * В методе осуществляется переход между страницами Входящие/Отправленные
     * и проверка на наличие название в title при открытии страницы.
     * Результат выводиться в консоль
     * @param text- название Входящие/Отправленные
     */
    public void navigateBoxesMail(String text) {
        (new WebDriverWait(driver, 60)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(By.partialLinkText(text)).isDisplayed();
            }
        });

        WebElement element = driver.findElement(By.partialLinkText(text));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);

        (new WebDriverWait(driver, 60)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.findElement(By.partialLinkText(text)).isDisplayed();
            }
        });
        System.out.println("ЗАДАН ТЕКСТ =" + text);
        title = "";
        title = driver.getTitle();

        if (title.contains(text)) {
            System.out.format("Titlle: %s%n", driver.getTitle());
            System.out.format("В title содержиться ожидаемый заголовок %s%n", text);

        } else {
            System.out.format("Titlle: %s%n", driver.getTitle());
            System.out.format("В title НЕ содержиться ожидаемый заголовок %s%n", text);

        }

    }

}



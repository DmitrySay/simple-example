package com.myapp.form;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by Acer on 10.11.2016.
 */
public class Task1ResultForm {


    private WebDriver driver;
    private By searchResultsLocator = By.xpath("//div[@class='search-i']/ol/descendant::li//h3/a[2]");

    public Task1ResultForm(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * В методе осуществляется проверка результатов поиска на соответствие
     * заданному регулярному выражению regexp. Метод возвращает список строк,
     * соответствующих регулярному выражению. Проверка осуществляется только для
     * первой страницы результатов поиска.
     *
     * @param regexp регулярное выражение для проверки результатов поиска
     * @return список соответствующих результатов поиска
     */

    public List<String> verifySearchResults(String regexp) {
        Pattern pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
        //Pattern.CASE_INSENSITIVE. CASE_INSENSITIVE означает, что при поиске выражения, регистр символов не будет учитываться.
        List<WebElement> searchResults = driver.findElements(searchResultsLocator);
        List<String> compilantResults = new ArrayList<String>();
        for (WebElement element : searchResults) {

            if (pattern.matcher(element.getText()).matches()) {
                compilantResults.add(element.getText());

            }
        }
        return compilantResults;
    }

    /**
     * В методе происходит открытие новой страницы, переход на нее
     * и закрытие страницы при существовании ссылки с абсолютным
     * соответствием заданному выражению searchexp.Иначе, бросается исключение
     * @param searchexp
     */
    public void findAndGoToSearchExp(String searchexp) {

        try {
            String originalWindow = driver.getWindowHandle();
            final Set<String> oldWindowsSet = driver.getWindowHandles();

            //переход по ссылке, если есть заданное выражение
            driver.findElement(By.linkText(searchexp)).click();

            String newWindow = (new WebDriverWait(driver, 10))
                    .until(new ExpectedCondition<String>() {
                               public String apply(WebDriver driver) {
                                   Set<String> newWindowsSet = driver.getWindowHandles();
                                   newWindowsSet.removeAll(oldWindowsSet);
                                   return newWindowsSet.size() > 0 ?
                                           newWindowsSet.iterator().next() : null;
                               }
                           }
                    );

            driver.switchTo().window(newWindow);

            System.out.println("New window title: " + driver.getTitle());
            driver.close();

            driver.switchTo().window(originalWindow);
            System.out.println("Old window title: " + driver.getTitle());

        } catch (WebDriverException e) {
            System.out.println(e);
            System.out.println("Результат не найден, броcили org.openqa.selenium.WebDriverException");
        }
    }

}



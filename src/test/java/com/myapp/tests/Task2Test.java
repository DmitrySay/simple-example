package com.myapp.tests;

import com.myapp.form.Task2LoginForm;
import com.myapp.form.Task2MailForm;
import com.myapp.form.Task2MainForm;
import com.myapp.form.Task2PasswordForm;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Acer on 10.11.2016.
 */
public class Task2Test {

    private WebDriver driver;

    /**
     * Конфигурирование теста: запуск браузера и переход к главной странице сайта
     *
     * @param appUrl-URL к главной странице
     */
    @BeforeTest
    @Parameters({"appUrl"})
    public void setUp(String appUrl) {
        driver = new FirefoxDriver();
        System.out.format("%n Открыли браузер%n");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);
        driver.manage().window().maximize();
        driver.get(appUrl);
        System.out.format("Перешли на страницу \"%s\"%n", appUrl);

    }

    /**
     * Очистка конфигурации по завершении теста: закрытие браузера
     */
    @AfterTest
    public void tearDown() {
        System.out.format("Закрыли браузер%n");
        driver.close();
    }

    @Test
    @Parameters({"login", "password", "email", "topic", "message"})
    public void taskTwoSearchResults(String login, String password, String email, String topic, String message) {

        System.out.println("Задание: Открыть в браузере почту гугл, залогиниться в нее с корректным аккаунтом");
        Task2MainForm mainForm = new Task2MainForm(driver);
        mainForm.navigateToMail();

        Task2LoginForm loginForm = new Task2LoginForm(driver);
        loginForm.setLogin(login);
        Task2PasswordForm passwordForm = new Task2PasswordForm(driver);
        passwordForm.setPassword(password);

        Task2MailForm mailForm = new Task2MailForm(driver);

        System.out.println("Задание: Написать и отослать письмо кому-либо");
        mailForm.setEmailTopicMessageAndSend(email, topic, message);

        System.out.println("Задание: Перейти в разделы входящие, исходящие...");
        mailForm.navigateBoxesMail("Отправленные");
        mailForm.navigateBoxesMail("Входящие");

         System.out.println("Задание: Вылогиниться из почты");
         mailForm.logoutFromMail();
    }

}


package com.myapp.tests;

import com.myapp.form.Task1MainForm;
import com.myapp.form.Task1ResultForm;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Acer on 10.11.2016.
 */
public class Task1Test {

    private WebDriver driver;

    /**
     * Конфигурирование теста: запуск браузера и переход к главной странице сайта
     *
     * @param appUrl-URL к главной странице
     */
    @BeforeTest
    @Parameters({"appUrl"})
    public void setUp(String appUrl) {
        driver = new FirefoxDriver();
        System.out.format("Открыли браузер%n");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);
        driver.manage().window().maximize();
        driver.get(appUrl);
        System.out.format("Перешли на страницу \"%s\"%n", appUrl);

    }

    /**
     * Очистка конфигурации по завершении теста: закрытие браузера
     */
    @AfterTest
    public void tearDown() {
        System.out.format("Закрыли браузер%n");
        driver.close();
    }

    @Test
    @Parameters({"request", "regexp", "searchexp"})
    public void taskOneSearchResults(String request, String regexp, String searchexp) {

        Task1MainForm mainForm = new Task1MainForm(driver);
        System.out.format("Получили доступ к главной странице%n");

        System.out.format("В поле поиска вводим %s%n", request);
        mainForm.search(request);

        Task1ResultForm task1ResultForm = new Task1ResultForm(driver);
        System.out.format("Получили доступ к странице с результатами поиска%n");

        List<String> results = task1ResultForm.verifySearchResults(regexp);
        System.out.format("Проверили соответствие результатов поиска запросу %s%n", regexp);

        if (results.isEmpty()) {
            System.out.format("%nНет результатов, соответствующих запросу%n");
        }

        String testResult = String.format("%nНайдено %d таких результатов, соответствующих запросу:%n", results.size());
        for (String result : results) {
            testResult = String.format("%s%s%n", testResult, result);
        }
        System.out.format("%s%n", testResult);

        System.out.format("Проверяем соответствие результатов поиска запросу %s%n", searchexp);
        task1ResultForm.findAndGoToSearchExp(searchexp);

    }


}


package com.myapp.tests;

import com.myapp.form.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Acer on 11.11.2016.
 */
public class Task3Test {


    private WebDriver driver;

    /**
     * Test configuration: start of the browser and transition to the MainPage of the website
     *
     * @param appUrl-URL for MainPage
     */
    @BeforeTest
    @Parameters({"appUrl"})
    public void setUp(String appUrl) {
        driver = new FirefoxDriver();
        System.out.format("%n Browser has opened %n");
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
        driver.manage().window().maximize();
        driver.get(appUrl);
        System.out.format("Browser has been opened at the page \"%s\"%n", appUrl);

    }

    /**
     * Cleaning of a configuration and closing of the browser after completion of the test
     */
    @AfterTest
    public void tearDown() {
        System.out.format("%n Close browser%n");
        driver.close();

    }

    @Test
    @Parameters({"flightType", "from", "to", "departDate", "returnDate", "travelDate", "showPriceIn"})
    public void taskOneSearchResults(String flightType,
                                     String from,
                                     String to,
                                     String departDate,
                                     String returnDate,
                                     String travelDate,
                                     String showPriceIn) {


        System.out.println("/Step 2 - Home page | Booking widget");
        Task3MainForm mainForm = new Task3MainForm(driver);

        mainForm.clickOnFlightTab();

        if (flightType.equals("ROUND TRIP")) {
            mainForm.clickOnRoundTrip();
        }

        mainForm.setFromAndToFields(from, to);
        mainForm.setDepartDateAndReturnDate(departDate, returnDate);

        if (travelDate.equals("EXACT DATES")) {
            mainForm.clickOnExactDates();
        }
        if (showPriceIn.equals("Money")) {
            mainForm.clickOnShowPriceInMoney();
        }
        mainForm.clickOnFindFlightsSubmit();

        System.out.println("/Step 3 - Tickets selection page");
        Task3TicketsForm ticketsForm = new Task3TicketsForm(driver);
        ticketsForm.selectTicket();
        ticketsForm.selectTicket();

        System.out.println("/Step 4 - Trip Summary page");
        Task3TripSummaryForm tripSummaryForm = new Task3TripSummaryForm(driver);
        tripSummaryForm.clickOnBtnContinue();

        System.out.println("/Step 5 - Passenger Info page");
        Task3PassInfoForm passInfoForm = new Task3PassInfoForm(driver);

        passInfoForm.fillDefaultFieldsInPassInfoPage();

        //6.1-Click on 'Review + Pay' button Не видел такой кнопки

        System.out.println("/Step 7 - Credit Card Info page");
        Task3CreditCardForm creditCardForm = new Task3CreditCardForm(driver);
        creditCardForm.checkIfBtnActive();
    }
}

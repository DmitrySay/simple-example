package com.myapp;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.IOException;

public class FTPCreateDirDemo {

    private static void showServerReply(FTPClient ftpClient) {
        String[] replies = ftpClient.getReplyStrings();
        if (replies != null && replies.length > 0) {
            for (String aReply : replies) {
                System.out.println("SERVER: " + aReply);
            }
        }
    }

    public static void main(String[] args) {
        String server = "ftp.fbo.gov";
        //One more ("ftp.dlink.ru");

        String user = "anonymous";
        String pass = "";
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server);

            ftpClient.enterLocalPassiveMode();

            showServerReply(ftpClient);
            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                System.out.println("Operation failed. Server reply code: " + replyCode);
                return;
            }
            boolean success = ftpClient.login(user, pass);
            showServerReply(ftpClient);
            if (!success) {
                System.out.println("Could not login to the server");
                return;
            }
            //Get all directories and open/close each one
            FTPFile[] files = ftpClient.listFiles();
            for (FTPFile file : files) {
                String details = file.getName();
                if (file.isDirectory()) {
                    System.out.println(details);

                    success = ftpClient.changeWorkingDirectory(details);
                    showServerReply(ftpClient);

                    if (success) {
                        System.out.println("Successfully changed working directory.");
                    } else {
                        System.out.println("Failed to change working directory. See server's reply.");
                    }

                    success = ftpClient.changeToParentDirectory();
                    if (success) {
                        System.out.println("Successfully BACK-changed working directory.");
                    } else {
                        System.out.println("Failed to BACK-changed working directory. See server's reply.");
                    }
                }
            }

            // Creates a directory
            String dirToCreate = "/test";
            success = ftpClient.makeDirectory(dirToCreate);
            showServerReply(ftpClient);
            if (success) {
                System.out.println("Successfully created directory: " + dirToCreate);
            } else {
                System.out.println("Failed to create directory. See server's reply.");
            }
            // Delete a directory
            String fileToDelete = "/test";
            boolean deleted = ftpClient.deleteFile(fileToDelete);
            if (deleted) {
                System.out.println("The file was deleted successfully.");
            } else {
                System.out.println("Could not delete the  file, it may not exist.");
            }

        } catch (IOException ex) {
            System.out.println("Oops! Something wrong happened");
            ex.printStackTrace();
        } finally {

            // logs out and disconnects from server
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }
}

